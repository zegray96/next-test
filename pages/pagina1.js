import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react'

export default function Pagina1() {
    const [value, setValue] = useState(0);
    const router = useRouter();

    useEffect(() => {
        if (router.isReady) {
            console.log(router.query);
        }
    }, [router])

    return (
        <>
            <div>Pagina 1</div>
            <div style={{ marginBottom: '20px', marginTop: '20px', fontSize: '20px', fontWeight: 'bold' }}>Valor de estado {value}</div>
            <button onClick={() => setValue(value + 1)}>Sumar 1</button>
        </>
    )
}
